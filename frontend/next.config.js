const withCSS = require('@zeit/next-css')
const trackerConfig = require('./app.config.json')

module.exports = withCSS({
  env: {
    ...trackerConfig
  }
})