import React from 'react'
import styled from '@emotion/styled'

const LogoContainer = styled.div`
  width: 50%;
  margin: auto;
`

type LogoProps = {
  src: string;
}

const Logo: React.FC<LogoProps> = (props: LogoProps) => {
  return (
    <LogoContainer>
      <img src={props.src} alt="Shahid Buttar for CA-12 2020" />
    </LogoContainer>
  )
}

export default Logo
