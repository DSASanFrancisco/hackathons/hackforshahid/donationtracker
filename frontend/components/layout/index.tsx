import React from 'react'
import 'modern-css-reset'
import Head from 'next/head'
import styled from '@emotion/styled'
import GlobalStyles from './global-styles'
import { NextSeo } from 'next-seo'

const LayoutStyle = styled.div`
  padding: 1rem;
  min-height: 100%;
  max-width: 40rem;
  margin: auto;
  text-align: center;
`

const Layout: React.FC = ({ children }) => {
  return (
    <React.Fragment>
      <NextSeo
        title="Shahid for Congress 2020 - "
        description="San Francisco deserves a representative in Congress who will champion climate justice, human rights, and racial justice. #ShahidVsPelosi"
        twitter={{
          handle: '@ShahidForChange',
          site: 'https://shahidforchange.us/',
          cardType: 'summary_large_image',
        }}
      />
      <Head>
        <link rel="icon" href="https://shahidforchange.us/wp-content/uploads/2019/04/favicon-32x32.png" sizes="32x32" />
      </Head>
      <GlobalStyles />
      <LayoutStyle>
        { children }
      </LayoutStyle>
    </React.Fragment>
  )
}

export default Layout
