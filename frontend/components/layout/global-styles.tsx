import React from 'react'
import { css, Global } from '@emotion/core'

const GlobalStyles: React.FC = () => (
  <Global styles={css`
      html {
        height: 100%;
      }

      body {
        height: 100%;
        background: ${process.env.backgroundColor};
        font-family: "Helvetica Neue", HelveticaNeue, "TeX Gyre Heros", TeXGyreHeros, FreeSans, "Nimbus Sans L", "Liberation Sans", Arimo, Helvetica, Arial, sans-serif;
      }

      body > div {
        height: 100%;
      }
    `}
  />
)

export default GlobalStyles
