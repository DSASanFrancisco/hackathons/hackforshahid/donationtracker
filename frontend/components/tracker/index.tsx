import React from 'react'
import moneyFormat from '../../util/money-format'
import { differenceInDays, addDays } from 'date-fns'
import { TrackerSegmentStyle, TrackerContainer, TrackerSegmentsStyle, TrackerStyle, TrackerBarStyle } from './style'

type TrackerProps = {
  currentAmount: number;
  targetAmount: number;
  targetDate: string;
  donors: number;
  segments?: number;
}

const Tracker: React.FC<TrackerProps> = ({
  segments,
  targetAmount,
  currentAmount,
  donors,
  targetDate
}: TrackerProps) => {
  const percentComplete = currentAmount / targetAmount * 100
  const levels = []
  const levelSegments = segments || 1
  const daysToGoal = differenceInDays(addDays(new Date(targetDate), 1), new Date())

  for (let i = 0; i < levelSegments; i += 1) {
    levels.push(targetAmount / levelSegments * (i + 1))
  }

  const renderedSegments = levels.map(amount => (
  <TrackerSegmentStyle key={amount}>
    ${moneyFormat(amount)}
  </TrackerSegmentStyle>
  ))

  return (
    <TrackerContainer>
      <TrackerSegmentsStyle>
        {renderedSegments}
      </TrackerSegmentsStyle>
      <TrackerStyle>
        <TrackerBarStyle percentComplete={percentComplete} />
      </TrackerStyle>
      ${moneyFormat(currentAmount)} raised of ${moneyFormat(targetAmount)} from {donors} donors
      <div>
        {daysToGoal > 0 && `${daysToGoal} days to go!`}
      </div>
    </TrackerContainer>
  )
}

export default Tracker
