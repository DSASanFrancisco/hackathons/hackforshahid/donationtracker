import styled from '@emotion/styled'

export const TrackerContainer = styled.div`
  margin: 2rem 0;
`
export const TrackerStyle = styled.div`
  border: 2px solid ${process.env.primaryColor};
  height: 2rem;
  width: 100%;
`
type TrackerBarProps = {
  percentComplete: number;
}
export const TrackerBarStyle = styled.div<TrackerBarProps> `
  background-color: ${process.env.primaryColor};
  width: ${(props: TrackerBarProps) => props.percentComplete}%;
  height: 100%;
`
export const TrackerSegmentsStyle = styled.div`
  display: flex;
  width: 100%;
`
export const TrackerSegmentStyle = styled.div`
  flex: 1;
  border-right: 2px solid ${process.env.primaryColor};
  text-align: right;
  padding: 0 .5rem;
`
