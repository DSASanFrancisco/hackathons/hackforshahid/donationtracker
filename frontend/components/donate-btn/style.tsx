import styled from '@emotion/styled'
export const DonateButtonStyle = styled.a`
  color: white;
  background: ${process.env.donateButtonColor};
  padding: .75rem 1.5rem;
  border-radius: 5px;
  text-decoration: none;
  font-weight: bold;
`
