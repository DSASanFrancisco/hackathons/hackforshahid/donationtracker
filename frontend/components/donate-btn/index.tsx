import React from 'react'
import { DonateButtonStyle } from './style'

const DonateButton: React.FC = () => {
  return (
    <DonateButtonStyle
      href={process.env.donationLink}
    >
      Donate
    </DonateButtonStyle>
  )
}

export default DonateButton
