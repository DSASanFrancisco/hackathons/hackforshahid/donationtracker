import React, { useEffect, useState } from 'react'
import Layout from '../components/layout'
import Tracker from '../components/tracker'
import DonateButton from '../components/donate-btn'
import Logo from '../components/logo'
import ApolloClient, { gql } from 'apollo-boost'

const DONATION_QUERY = gql`
{
  target {
    totalDollarAmount
  }
  totals {
    dollarAmount
    numberOfDonations
  }
}
`

const Home = () => {
  const [ data, setData ] = useState({
    currentAmount: 0,
    targetAmount: 0,
    totalDonors: 0,
    loaded: false,
  })
  useEffect(() => {
    const client = new ApolloClient({
      uri: `${process.env.apiPath}/api/graphql`
    })

    client.query({
      query: DONATION_QUERY
    }).then(resp => {
      setData({
        currentAmount: resp.data.totals.dollarAmount,
        targetAmount: resp.data.target.totalDollarAmount,
        totalDonors: resp.data.totals.numberOfDonations,
        loaded: true,
      })
    })
  }, [])
  return (
    <Layout>
      <Logo src={process.env.logoUrl} />
      {data.loaded && <Tracker
        currentAmount={data.currentAmount}
        targetAmount={data.targetAmount}
        targetDate={process.env.targetDate}
        donors={data.totalDonors}
        segments={4}
      />}
      <DonateButton />
    </Layout>
  )
}

export default Home
