export default (amt: number, decimals: number = 0) => amt.toFixed(decimals).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")
