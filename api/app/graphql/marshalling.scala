package graphql

import io.circe._
import sangria.marshalling._

// The code copied from the circe-sangria project. Unfortunately, this code is not compiled against Scala 2.13
//
// The original code can be found here:
// https://github.com/circe/circe-sangria/blob/master/src/main/scala/io/circe/sangria/package.scala

object marshalling {

  implicit object CirceResultMarshaller extends ResultMarshaller {
    override type Node = Json
    override type MapBuilder = ArrayMapBuilder[Node]

    override def emptyMapNode(keys: Seq[String]): ArrayMapBuilder[Json] = new ArrayMapBuilder(keys)
    override def addMapNodeElem(builder: MapBuilder, key: String, value: Node, optional: Boolean): ArrayMapBuilder[Json] = {
      builder.add(key, value)
    }

    override def mapNode(builder: MapBuilder): Json = Json.fromFields(builder)
    override def mapNode(keyValues: Seq[(String, Json)]): Json = Json.fromFields(keyValues)

    override def arrayNode(values: Vector[Json]): Json = Json.fromValues(values)
    override def optionalArrayNodeValue(value: Option[Json]): Json = value.getOrElse(nullNode)

    override def scalarNode(value: Any, typeName: String, info: Set[ScalarValueInfo]): Json = value match {
      case v: String => Json.fromString(v)
      case v: Boolean => Json.fromBoolean(v)
      case v: Int => Json.fromInt(v)
      case v: Long => Json.fromLong(v)
      case v: Float => Json.fromDoubleOrNull(v)
      case v: Double => Json.fromDoubleOrNull(v)
      case v: BigInt => Json.fromBigInt(v)
      case v: BigDecimal => Json.fromBigDecimal(v)
      case v => throw new IllegalArgumentException("Unsupported scalar value: " + v)
    }

    override def enumNode(value: String, typeName: String): Json = Json.fromString(value)

    override def nullNode: Json = Json.Null

    override def renderCompact(node: Json): String = node.noSpaces
    override def renderPretty(node: Json): String = node.spaces2
  }

  implicit object CirceMarshallerForType extends ResultMarshallerForType[Json] {
    override final val marshaller = CirceResultMarshaller
  }

  implicit object CirceInputUnmarshaller extends InputUnmarshaller[Json] {
    override def getRootMapValue(node: Json, key: String): Option[Json] = node.asObject.get(key)

    override def isMapNode(node: Json): Boolean = node.isObject
    override def getMapValue(node: Json, key: String): Option[Json] = node.asObject.get(key)
    override def getMapKeys(node: Json): Iterable[String] = node.asObject.get.keys

    override def isListNode(node: Json): Boolean = node.isArray
    override def getListValue(node: Json): Vector[Json] = node.asArray.get

    override def isDefined(node: Json): Boolean = !node.isNull
    override def getScalarValue(node: Json): Any = {
      def invalidScalar = throw new IllegalStateException(s"$node is not a scalar value")

      node.fold(
        jsonNull = invalidScalar,
        jsonBoolean = identity,
        jsonNumber = num => num.toBigInt orElse num.toBigDecimal getOrElse invalidScalar,
        jsonString = identity,
        jsonArray = _ => invalidScalar,
        jsonObject = _ => invalidScalar
      )
    }

    override def getScalaScalarValue(node: Json): Any = getScalarValue(node)

    override def isEnumNode(node: Json): Boolean = node.isString

    override def isScalarNode(node: Json): Boolean =
      node.isBoolean || node.isNumber || node.isString

    override def isVariableNode(node: Json) = false
    override def getVariableName(node: Json) = throw new IllegalArgumentException("variables are not supported")

    override def render(node: Json): String = node.noSpaces
  }

  implicit object circeToInput extends ToInput[Json, Json] {
    override final def toInput(value: Json): (Json, InputUnmarshaller[Json]) = (value, CirceInputUnmarshaller)
  }

  implicit object circeFromInput extends FromInput[Json] {
    override final val marshaller = CirceResultMarshaller
    override final def fromResult(node: marshaller.Node): Json = node
  }

  implicit def circeEncoderToInput[T : Encoder]: ToInput[T, Json] = {
    value => implicitly[Encoder[T]].apply(value) -> CirceInputUnmarshaller
  }

  implicit def circeDecoderFromInput[T : Decoder]: FromInput[T] = {
    new FromInput[T] {
      override final val marshaller = CirceResultMarshaller
      override final def fromResult(node: marshaller.Node): T = implicitly[Decoder[T]].decodeJson(node) match {
        case Right(obj) => obj
        case Left(error) => throw InputParsingError(Vector(error.getMessage))
      }
    }
  }
}
