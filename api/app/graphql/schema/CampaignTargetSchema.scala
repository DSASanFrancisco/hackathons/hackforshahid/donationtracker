package graphql.schema

import context.AppContext
import models.CampaignTarget
import sangria.schema._

object CampaignTargetSchema {

  lazy val RootType: ObjectType[AppContext, CampaignTarget] = ObjectType(
    "CampaignTarget",
    "Totals for the campaign goal",
    fields[AppContext, CampaignTarget](
      Field(
        "totalDollarAmount",
        IntType,
        Some("The target dollar amount for the campaign goal"),
        resolve = _.value.totalDollarAmount
      ),
    )
  )
}
