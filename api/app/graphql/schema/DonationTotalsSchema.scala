package graphql.schema

import context.AppContext
import models.DonationTotals
import sangria.schema._

object DonationTotalsSchema {

  lazy val RootType: ObjectType[AppContext, DonationTotals] = ObjectType(
    "DonationTotals",
    "Totals for the campaign goal",
    fields[AppContext, DonationTotals](
      Field(
        "dollarAmount",
        IntType,
        Some("Total dollar amount of donations"),
        resolve = _.value.dollarAmount
      ),
      Field(
        "numberOfDonations",
        IntType,
        Some("Total number of individual donations made"),
        resolve = _.value.numberOfDonations
      ),
    )
  )
}
