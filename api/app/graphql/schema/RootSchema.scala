package graphql.schema

import context.FullRequestContext
import sangria.schema._

object RootSchema {

  lazy val QueryType: ObjectType[FullRequestContext, Unit] = ObjectType(
    "QueryRoot",
    "Root object of all queries",
    fields[FullRequestContext, Unit](
      Field(
        "target",
        CampaignTargetSchema.RootType,
        Some("Donation totals for the current campaign"),
        resolve = _.ctx.services.campaigns.getTarget()
      ),
      Field(
        "totals",
        DonationTotalsSchema.RootType,
        Some("Donation totals for the current campaign"),
        resolve = _.ctx.services.campaigns.getTotals()
      ),
    )
  )

  lazy val RootType: Schema[FullRequestContext, Unit] = Schema(
    QueryType,
    description = Some("GraphQL API for accumulating donations and goals for political campaigns"),
  )
}
