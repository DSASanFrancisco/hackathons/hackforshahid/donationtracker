package graphql

import context.{AppContext, FullRequestContext, RequestContext, RequestData}
import graphql.marshalling._
import io.circe.Json
import modules.ServicesModule
import play.api.mvc.RequestHeader
import sangria.ast.Document
import sangria.execution.ExecutionScheme.Extended
import sangria.execution.{ExecutionResult, Executor}

import scala.concurrent.{ExecutionContext, Future}

class GraphQLExecutor(
  servicesModule: ServicesModule,
  executionContext: ExecutionContext
) {

  private implicit val ec: ExecutionContext = executionContext

  def execute(query: Document, opName: Option[String])(implicit rh: RequestHeader): Future[ExecutionResult[FullRequestContext, Json]] = {
    Executor.execute(
      schema.RootSchema.RootType,
      query,
      operationName = opName,
      root = (),
      userContext = new AppContext with RequestContext {
        override final val request: RequestData = RequestData(rh)
        override final def services: ServicesModule = servicesModule
      },
    )
  }

}
