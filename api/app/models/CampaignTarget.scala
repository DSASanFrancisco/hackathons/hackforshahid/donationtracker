package models

final case class CampaignTarget(
  totalDollarAmount: Int
)
