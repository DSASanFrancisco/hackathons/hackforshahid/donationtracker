package models

final case class DonationTotals(
  numberOfDonations: Int,
  dollarAmount: Int,
)
