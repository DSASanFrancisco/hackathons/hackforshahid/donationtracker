package services

import models.{CampaignTarget, DonationTotals}

import scala.concurrent.Future

class CampaignService() {

  def getTarget(): Future[CampaignTarget] = {
    Future.successful(CampaignTarget(200))
  }

  def getTotals(): Future[DonationTotals] = {
    Future.successful(DonationTotals(10, 150))
  }

  def addDonation(): Future[Unit] = Future.successful(())
}
