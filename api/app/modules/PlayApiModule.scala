package modules

import _root_.api.Routes
import com.softwaremill.macwire._
import controllers.AssetsComponents
import graphql.GraphQLExecutor
import play.api.mvc.EssentialFilter
import play.api.routing.Router
import play.api.{ApplicationLoader, BuiltInComponentsFromContext}
import play.filters.cors.CORSConfig.Origins
import play.filters.cors.{CORSConfig, CORSFilter}

class PlayApiModule(context: ApplicationLoader.Context)
  extends BuiltInComponentsFromContext(context)
  with AssetsComponents {

  lazy val controllers: ControllersModule = wire[ControllersModule]

  lazy val services: ServicesModule = wire[ServicesModule]

  lazy val graphQLExecutor: GraphQLExecutor = wire[GraphQLExecutor]

  override lazy val httpFilters: Seq[EssentialFilter] = Seq(
    CORSFilter(CORSConfig(Origins.All)) // Might not be needed if the API serves the UI
  )

  override lazy val router: Router = {
    val prefix = "/"
    wire[Routes]
  }
}
