package modules

import com.softwaremill.macwire._
import controllers.{AssetsBuilder, AssetsController, GraphQLController, RestController}
import graphql.GraphQLExecutor
import play.api.mvc.ControllerComponents

@Module
class ControllersModule(
  assets: AssetsBuilder,
  controllerComponents: ControllerComponents,
  graphQLExecutor: GraphQLExecutor,
) {

  lazy val assetsController: AssetsController = wire[AssetsController]

  lazy val graphQLController: GraphQLController = wire[GraphQLController]

  lazy val restController: RestController = wire[RestController]
}
