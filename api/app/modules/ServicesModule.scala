package modules

import com.softwaremill.macwire._
import services.CampaignService

@Module
class ServicesModule {

  lazy val campaigns: CampaignService = wire[CampaignService]
}
