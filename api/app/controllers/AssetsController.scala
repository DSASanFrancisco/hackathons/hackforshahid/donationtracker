package controllers

import play.api.mvc.{Action, AnyContent}

class AssetsController(assets: AssetsBuilder) {

  val graphiQL: Action[AnyContent] = assets.at("graphiql.html")
}
