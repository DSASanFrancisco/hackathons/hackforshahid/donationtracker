package controllers

import cats.syntax.show._
import graphql.GraphQLExecutor
import play.api.libs.circe.Circe
import play.api.mvc._
import sangria.parser.DeliveryScheme.Either
import sangria.parser.{QueryParser, SyntaxError}

import scala.concurrent.Future

class GraphQLController(
  components: ControllerComponents,
  executor: GraphQLExecutor
) extends AbstractController(components)
  with Circe {

  def get(
    query: String,
    operationName: Option[String]
  ): EssentialAction = Action.async(parse.empty) { implicit request =>
    processQuery(query, operationName)
  }

  val post: EssentialAction = Action.async(circe.tolerantJson) { implicit request =>
    val jsonBody = request.body
    jsonBody.hcursor.get[String]("query") match {
      case Right(queryString) =>
        val opName = Option(jsonBody.hcursor.get[String]("operationName").getOrElse("")).filterNot(_.isEmpty)
        processQuery(queryString, opName)
      case Left(err) =>
        Future.successful(BadRequest(s"Could not decode json: ${err.show}"))
    }
  }

  def processQuery(queryString: String, opName: Option[String])(implicit request: RequestHeader): Future[Result] = {
    val parsedInput = QueryParser.parse(queryString).left.map {
      case err: SyntaxError =>
        BadRequest(err.getMessage())
      case err =>
        BadRequest(s"Unexpected error while parsing GraphQL query. $err")
    }
    parsedInput match {
      case Left(result) => Future.successful(result)
      case Right(query) =>
        executor.execute(query, opName).map { executionResult =>
          Ok(executionResult.result)
        }(defaultExecutionContext)
    }
  }
}
