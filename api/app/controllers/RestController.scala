package controllers

import io.circe.Json
import play.api.libs.circe.Circe
import play.api.mvc.{AbstractController, Action, ControllerComponents}

import scala.concurrent.Future

class RestController(components: ControllerComponents)
  extends AbstractController(components)
    with Circe {

  val addDonation: Action[Json] = Action.async(circe.tolerantJson) { implicit request =>
    Future.successful(Ok(s"Hello World! Thanks for your ${request.body.spaces4}"))
  }
}
