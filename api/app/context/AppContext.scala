package context

import modules.ServicesModule

trait AppContext {
  def services: ServicesModule
}
