package context

import play.api.mvc.RequestHeader

trait RequestContext {

  def request: RequestData
}

final case class RequestData(
  request: RequestHeader
)
