package server

import modules.PlayApiModule
import play.api.{Application, ApplicationLoader, LoggerConfigurator}

class ApiApplicationLoader extends ApplicationLoader {

  override def load(context: ApplicationLoader.Context): Application = {
    // Configure logging
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment, context.initialConfiguration, Map.empty)
    }
    // Build the API
    val api = new PlayApiModule(context)
    // Return the Play Application
    api.application
  }
}
