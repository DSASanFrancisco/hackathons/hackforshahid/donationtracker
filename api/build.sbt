name := "donation-tracker-api"
organization := "org.dsasf"

version := "0.0.1"
scalaVersion := "2.13.1"

graphqlSchemaSnippet := "graphql.schema.RootSchema.RootType"
graphqlSchemaGen / target := baseDirectory.value

val circeSangriaVersion = "0.1.0"
val circeVersion = "0.12.3"
val playCirceVersion = "2712.0"
val macwireVersion = "2.3.3"
val sangriaVersion = "2.0.0-M1"
val sangriaCirceVersion = "1.2.1"
val scalaTestVersion = "3.1.0"
val zioVersion = "1.0.0-RC17"

enablePlugins(PlayScala, GraphQLSchemaPlugin)

libraryDependencies ++= Seq(
  "com.dripower" %% "play-circe" % playCirceVersion,
  "com.softwaremill.macwire" %% "macros" % macwireVersion % "provided", // only needed at compile-time
  "com.softwaremill.macwire" %% "util" % macwireVersion, // required at runtime
  "dev.zio" %% "zio" % zioVersion,
  "io.circe" %% "circe-core" % circeVersion,
  "org.sangria-graphql" %% "sangria" % sangriaVersion,
  "org.scalactic" %% "scalactic" % scalaTestVersion,
) ++ Seq(
  // Test-only dependencies
  "org.scalatest" %% "scalatest" % scalaTestVersion,
).map(_ % Test)
